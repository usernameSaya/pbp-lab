from django.db import models
from lab_2.models import Note
from django import forms


class NoteForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['To'].widget.attrs.update({
            'required':'',
            'name':'To',
            'id':'To',
            'type':'text',
            'class':'form-input',
            'placeholder':'Udin Samsuddin',
            'maxlength':'16',
            'minlength':'1'
        })
        self.fields['From'].widget.attrs.update({
            'required':'',
            'name':'From',
            'id':'From',
            'type':'text',
            'class':'form-input',
            'placeholder':'Budi Kawarman',
            'maxlength':'16',
            'minlength':'1'
        })
        self.fields['Title'].widget.attrs.update({
            'required':'',
            'name':'Title',
            'id':'Title',
            'type':'text',
            'class':'form-input',
            'placeholder':'Judul',
            'maxlength':'16',
            'minlength':'1'
        })
        self.fields['Message'].widget.attrs.update({
            'required':'',
            'name':'Message',
            'id':'Message',
            'type':'text',
            'class':'form-input',
            'placeholder':'Pesan',
            'maxlength':'300',
            'minlength':'1'
        })

    class Meta:
        model = Note
        fields = "__all__"
        widgets={
            'To' : forms.TextInput(attrs={'class':'form-control'}),
            'From' : forms.TextInput(attrs={'class':'form-control'}),
            'Title' : forms.TextInput(attrs={'class':'form-control'}),
            'Message' : forms.TextInput(attrs={'class':'form-control'}),
        }