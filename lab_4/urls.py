from django.urls import path
from django.conf.urls import url
from .views import index, add_note, note_list


urlpatterns = [
    path('', index, name='index_lab4'),
    path('add-note/', add_note, name='nambah'),
    path('note-list/', note_list, name='note_list'),
]