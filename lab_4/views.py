from django.shortcuts import redirect, render, HttpResponseRedirect
from .forms import NoteForm
from lab_2.models import Note

def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
    form = NoteForm(request.POST)

    if form.is_valid():
        To = form.cleaned_data.get("To");
        From = form.cleaned_data.get("From");
        Title = form.cleaned_data.get("Title");
        Message = form.cleaned_data.get("Message");
        form.save()
        # TO DO : ganti jadi redirect
        return redirect("/lab-4/")
    else:
        form = NoteForm()
        
    context['form'] = form
    return render(request, "lab4_form.html", context )

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)



# Create your views here.


