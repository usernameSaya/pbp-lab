from django.urls import path
from .views import index, get_note


urlpatterns = [
    path('', index, name='index_lab5'),
    path('notes/notes_id/', get_note),
]