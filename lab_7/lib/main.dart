import 'package:flutter/material.dart';
import 'article.dart';
import 'vaksin.dart';
import 'bed.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login',
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.blueGrey,
      ),
      home: const MyHomePage(title: 'HERMITAGE'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  int _currentIndex = 0;
  static List<Widget> _halaman = <Widget>[
    BedPage(),
    VaksinPage(),
    ArtikelPage()
  ];

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideDrawer(),
      backgroundColor: Color(0xFF10323A),
      appBar: AppBar(
        backgroundColor: Color(0xFF020215),
        title: TextButton(
            onPressed: () {},
            child: Row(
              children: const [
                Icon(
                  Icons.medical_services,
                  color: Colors.green,
                  size: 30.0,
                ),
                SizedBox(
                  width: 10.0,
                ),
                Text(
                  'HERMITAGE',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ],
            )),
      ),
      body: _halaman[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.airline_seat_individual_suite),
            label: 'Bed',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.assignment),
            label: 'Vaksin',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            label: 'Artikel',
          ),
        ],
      ),
    );
  }
}

class SideDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          DrawerHeader(
            child: Center(
              child: Text(
                'Mau Ke Mana?',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
            decoration: BoxDecoration(
              color: Color(0xFF10323A),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Register'),
            // onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.login_sharp),
            title: Text('Login'),
            onTap: () => {Navigator.of(context).pop()},
          ),
        ],
      ),
    );
  }
}
     
      // Padding(
      //   padding: const EdgeInsets.only(top: 120, left: 24, right: 24),
      //   child: Center(
      //     child: Column(
      //       children: [
      //         const SizedBox(height: 75),
      //         const Text(
      //           'Selamat Datang, Pengurus Rumah Sakit',
      //           style: TextStyle(fontSize: 20),
      //         ),
      //         const SizedBox(height: 40),
      //         Column(
      //           children: [
      //             TextFormField(
      //               style: const TextStyle(color: Colors.black),
      //               decoration: InputDecoration(
      //                   fillColor: const Color(0xffF1F0F5),
      //                   filled: true,
      //                   enabledBorder: OutlineInputBorder(
      //                     borderRadius: BorderRadius.circular(20),
      //                     borderSide: const BorderSide(),
      //                   ),
      //                   labelText: 'Username',
      //                   labelStyle: const TextStyle(color: Colors.black87)),
      //             ),
      //             const SizedBox(height: 20),
      //             TextFormField(
      //               style: const TextStyle(color: Colors.black),
      //               decoration: InputDecoration(
      //                   fillColor: const Color(0xffF1F0F5),
      //                   filled: true,
      //                   enabledBorder: OutlineInputBorder(
      //                     borderRadius: BorderRadius.circular(20),
      //                     borderSide: const BorderSide(),
      //                   ),
      //                   labelText: 'Password',
      //                   labelStyle: const TextStyle(color: Colors.black87)),
      //             ),
      //             const SizedBox(height: 20),
      //             ClipRRect(
      //               borderRadius: BorderRadius.circular(4),
      //               child: Stack(
      //                 children: <Widget>[
      //                   Positioned.fill(
      //                     child: Container(
      //                       decoration: const BoxDecoration(
      //                         gradient: LinearGradient(
      //                           colors: <Color>[
      //                             Color(0xFF2B7983),
      //                             Color(0xFF35939F),
      //                             Color(0xFF3ABA9B),
      //                           ],
      //                         ),
      //                       ),
      //                     ),
      //                   ),
      //                   TextButton(
      //                     style: TextButton.styleFrom(
      //                       padding: const EdgeInsets.all(16.0),
      //                       shape: RoundedRectangleBorder(
      //                         borderRadius: BorderRadius.circular(24.0),
      //                       ),
      //                       primary: Colors.white,
      //                       textStyle: const TextStyle(fontSize: 20),
      //                     ),
      //                     onPressed: () {},
      //                     child: const Text('SIGN IN'),
      //                   ),
      //                 ],
      //               ),
      //             ),
      //           ],
      //         )
      //       ],
      //     ),
      //   ),
      // ),