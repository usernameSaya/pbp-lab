# Perbedaan JSON dan XML
**JSON**

Dari segi syntax : Cenderung lebih mirip dengan tipe data dictionary Python.

Dari segi kegunaan : Cocok untuk digunakan pada pengantaran/transfer data yang sederhana.

Dari segi tipe data yang digunakan : Bisa menggunakan string, integer, array, dan boolean.

Dari segi readibility : Dapat dibaca antarbrowser

Dari segi pembuatan : Lebih msingkat dan mudah untuk dibuat karena berbentuk map/dictionary

**XML**

Dari segi syntax : Seperti HTML, dengan menggunakan tanda <..> (content) </..> untuk setiap elemen.

Dari segi kegunaan : Lebih banyak digunakan untuk melakukan penyimpanan data secara struktural dan terorganisir.

Dari segi tipe data yang digunakan : Hanya boleh menggunakan string.

Dari segi readibility : Dapat dibaca antarplatform

Dari segi pembuatan : Lebih lama  untuk dibuat karena strukturnya berbentuk tree

# Perbedaan HTML dan XML
**HTML**

Pada HTML, tag yang digunakan hanya boleh tag yang berasal dari HTML itu sendiri.

Tujuan utama HTML adalah untuk menampilkan data. 

HTML bersifat statis.


**XML**

Pada XML, tag boleh dibuat secara khusus sesuai dengan kebutuhan pengguna.

Di sisi lain, XML berfokus pada penyimpanan dan pengiriman data.

XML bersifat dinamis.
